const express = require("express");
const router = express.Router();
const userController = require('../controllers/userControllers');
const auth = require("../auth");

//Route for User Registration
router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController));
})

//Route for User Authentication
router.post("/login",(req,res)=>{
	userController.loginUser(req.body).then(resultFromController=>res.send(resultFromController))
});

//Route for Making User Admin

router.put("/updateAdmin/:id",auth.verify,(req,res)=>{

	const userData = auth.decode(req.headers.authorization);

	userController.makeAdmin({userId:userData.id}).then(resultFromController=>res.send(resultFromController));
});




module.exports = router;