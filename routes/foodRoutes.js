const express = require("express");
const router = express.Router();
const foodController = require("../controllers/foodControllers");
const auth = require("../auth");

//Create a food item

router.post("/",auth.verify,(req,res)=>{

	const data ={
		food: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data)
	foodController.createFood(data).then(resultFromController=>res.send(resultFromController))
});

//Route to Retrieve all active food items

router.get("/selection",(req,res)=>{
	foodController.getAllActive().then(resultFromController=>res.send(resultFromController))
});

//Route to Retrieve all food

router.get("/allfoodselection",(req,res)=>{
	foodController.getAllFood().then(resultFromController=>res.send(resultFromController))
});


//Route to Retrieve single food

router.get("/:foodId",(req,res)=>{
	foodController.getFood(req.params).then(resultFromController=>res.send(resultFromController));
})

//Route to Update Food item information (Admin only)

router.put("/:foodId",auth.verify,(req,res)=>{
	foodController.updateFood(req.params,req.body).then(resultFromController=>res.send(resultFromController));
});

//Archive Food (Admin only)

router.put("/:foodId/archive",auth.verify,(req,res)=>{
	foodController.archiveFood(req.params,req.body).then(resultFromController=>res.send(resultFromController));
});

//Route to activate food (admin only)

router.put("/:foodId/activate",auth.verify,(req,res)=>{
	foodController.activateFood(req.params,req.body).then(resultFromController=>res.send(resultFromController));
});


module.exports = router;