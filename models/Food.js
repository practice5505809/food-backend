const mongoose = require("mongoose");

const foodSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "Food item's name is required"]
	},
	description : {
		type : String,
		required : [true, "Description is required"]
	},
	imageUrl: {
    type: String,
    default: 'https://i.stack.imgur.com/yZlqh.png',
 	},
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type : Date,
		default : new Date()
	}

});


module.exports = mongoose.model("Food", foodSchema);
