const Food = require("../models/Food");
const User = require("../models/User");

// Create Food (Admin Only)

module.exports.createFood = (data) =>{
	let newFood = new Food({
		name : data.food.name,
		description : data.food.description,
		price : data.food.price
	})

	return newFood.save().then((food,error)=>{
		if(error){
			return false;
		}else{
			return { message: "Food Item successfully added!" };
		};
	});
};


//Retrieve all active food

module.exports.getAllActive =()=>{
	return Food.find({isActive: true}).then(result=>{
			return result
})}

//Retrieve all food items (active and not active)

module.exports.getAllFood =()=>{
	return Food.find({}).then(result=>{
			return result
})}

//Retrieve single food item

module.exports.getFood = (reqParams)=>{
	return Food.findById(reqParams.foodId).then(result=>{
		return result;
	});
};

//Update Food information (Admin only)

module.exports.updateFood = (reqParams,reqBody) =>{
	let updatedFood ={
		name: reqBody.name,
		description: reqBody.description,
		price:reqBody.price
	};
	return Food.findByIdAndUpdate(reqParams.foodId,updatedFood).then((food,error)=>{
		if(error){
			return false;
		}else{
			return "Food information updated!";
		};
	});
};

//Archive Food (Admin only)

module.exports.archiveFood = (reqParams,reqBody) =>{
	let archiveFood ={
		isActive : reqBody.isActive
	};
	return Food.findByIdAndUpdate(reqParams.productId,archiveFood).then((food,error)=>{
		if(error){
			return false;
		}else{
			return { message: "Food Item is now archived!" }
		};
	});
};

//Activate Food (Admin only)

module.exports.activateFood = (reqParams,reqBody) =>{
	let activateFood ={
		isActive : reqBody.isActive
	};
	return Food.findByIdAndUpdate(reqParams.foodId,activateFood).then((food,error)=>{
		if(error){
			return false;
		}else{
			return {message: "Food is now active"};
		};
	});
};



