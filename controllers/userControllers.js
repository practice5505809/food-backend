const User = require("../models/User");
const Food = require("../models/Food");
const bcrypt = require("bcrypt");
const auth = require("../auth");


//User Registration

module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password,10)
	})

	return newUser.save().then((result,error)=>{
		if(error){
			return false;
		}else{
			return 'Successfully registered, Welcome!';
		};
	});
};

//User Authentication

module.exports.loginUser = (reqBody) =>{
	return User.findOne({email:reqBody.email}).then(result=>{
		if(result==null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password);
			if(isPasswordCorrect){
				return {
					userId: result._id.valueOf(),
				access: auth.createAccessToken(result)}
			}else{ 
				return false
			};
		};
	});
};

//Make User Admin

module.exports.makeAdmin = (data) =>{
	return User.findById(data.userId).then(result=>{
		result.isAdmin = true;
		return result.save();
	})
}




